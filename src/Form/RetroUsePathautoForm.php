<?php

namespace Drupal\retro_use_pathauto\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Configure file system settings for this site.
 */
class RetroUsePathautoForm extends FormBase {


  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'retro_use_pathauto_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    retro_use_pathauto_set_nodes();
    $form = [];

    $form['#update_callbacks'] = [];

    $form['update'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select the types of paths for which to generate URL aliases'),
      '#options' => [],
      '#default_value' => [],
    ];


    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Update'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $batch = [
      'title' => $this->t('Bulk updating URL aliases'),
      'operations' => [
        ['Drupal\pathauto\Form\PathautoBulkUpdateForm::batchStart', []],
      ],
      'finished' => 'Drupal\pathauto\Form\PathautoBulkUpdateForm::batchFinished',
    ];

    $action = $form_state->getValue('action');

    foreach ($form_state->getValue('update') as $id) {
      if (!empty($id)) {
        $batch['operations'][] = ['Drupal\pathauto\Form\PathautoBulkUpdateForm::batchProcess', [$id, $action]];
      }
    }

    batch_set($batch);
  }

  /**
   * Batch callback; initialize the number of updated aliases.
   */
  public static function batchStart(&$context) {
    $context['results']['updates'] = 0;
  }

  /**
   * Common batch processing callback for all operations.
   *
   * Required to load our include the proper batch file.
   */
  public static function batchProcess($id, $action, &$context) {
    /** @var \Drupal\pathauto\AliasTypeBatchUpdateInterface $alias_type */
    // $alias_type = \Drupal::service('plugin.manager.alias_type')->createInstance($id);
    // $alias_type->batchUpdate($action, $context);
  }

  /**
   * Batch finished callback.
   */
  public static function batchFinished($success, $results, $operations) {
    if ($success) {
      if ($results['updates']) {
        \Drupal::service('messenger')->addMessage(\Drupal::translation()
          ->formatPlural($results['updates'], 'Generated 1 URL alias.', 'Generated @count URL aliases.'));
      }
      else {
        \Drupal::service('messenger')
          ->addMessage(t('No new URL aliases to generate.'));
      }
    }
    else {
      $error_operation = reset($operations);
      \Drupal::service('messenger')
        ->addMessage(t('An error occurred while processing @operation with arguments : @args'), [
          '@operation' => $error_operation[0],
          '@args' => print_r($error_operation[0]),
        ]);
    }
  }

}
