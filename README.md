Retroactively Use Automatic Path Aliases

Allows administrator to force already-created content to be set to use automatic aliases, so Pathauto can bulk generate path aliases based on its patterns for these old nodes.
